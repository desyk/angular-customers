export const environment = {
  production: true,
  firebaseApiKey: 'AIzaSyAAAVGaD_mNvwRfdmUWLE727KjUi1Q59Qo',
  firebaseNewUserPath: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=',
  firebaseLoginPath: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=',
  customersFirebaseUrl: 'https://angular-project-me.firebaseio.com/customers.json',
  statesFirebaseUrl: 'https://angular-project-me.firebaseio.com/states.json',
  googleMapsApiKey: 'AIzaSyAt8tjtFZK0nRK2yXwf3HB1mS4FRw0HJ9c',
  googleMapsGeoAddress: 'https://maps.googleapis.com/maps/api/geocode/json',
};
